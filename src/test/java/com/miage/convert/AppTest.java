package com.miage.convert;

import com.miage.convert.config.MessageConfig;
import com.miage.convert.implementation.ReportServiceImplementation;
import com.miage.convert.service.ReportServiceInterface;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	
	private ReportServiceInterface reportService;
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }
    
    @Override
	protected void setUp() {
    	this.reportService = new ReportServiceImplementation();
    }
    
    public void testValidateReferenceNumReference() {
    	assertEquals(MessageConfig.NUMREFERENCE_INVALID, reportService.validateReference("146210040;B;105.23;97"));
    }
    
    public void testValidateReferenceColor() {
    	assertEquals(MessageConfig.COLOR_INVALID, reportService.validateReference("1462100403;A;100.1;9"));
    }
    
    public void testValidateReferencePrice() {
    	assertEquals(MessageConfig.PRICE_INVALID, reportService.validateReference("1462100044;G;5,56;19"));
    }
    
    public void testValidateReferenceSize() {
    	assertEquals(MessageConfig.SIZE_INVALID, reportService.validateReference("1460900848;G;12.0;145.5"));
    }
    
    public void testValidateReferenceNumElements() {
    	assertEquals(MessageConfig.NUMELEMENTS_INVALID, reportService.validateReference("1460100040;R;45.12"));
    }
    
    public void testValidateReferenceValide() {
    	assertEquals(MessageConfig.REFERENCE_VALID, reportService.validateReference("1460100040;R;45.12;27"));
    }
       
}
