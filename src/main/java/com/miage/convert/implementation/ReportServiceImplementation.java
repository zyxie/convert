package com.miage.convert.implementation;

import com.miage.convert.service.ReportServiceInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.miage.convert.config.MessageConfig;
import com.miage.convert.config.RegexConfig;
import com.miage.convert.model.Report;
import com.miage.convert.model.Mistake;
import com.miage.convert.model.Reference;

public class ReportServiceImplementation implements ReportServiceInterface {

	public String validateReference(String str) {
		
		String message = new String();	
		String[] parts = str.split(";");
		if (parts.length != 4) {
			message = MessageConfig.NUMELEMENTS_INVALID;
		}
		else if (!this.validateElement(parts[0], RegexConfig.NUMREFERENCE_REGEX)) {
			message = MessageConfig.NUMREFERENCE_INVALID;
		}
		else if (!this.validateElement(parts[1], RegexConfig.COLOR_REGEX)) {
			message = MessageConfig.COLOR_INVALID;
		} 
		else if (!this.validateElement(parts[2], RegexConfig.PRICE_REGEX)) {
			message = MessageConfig.PRICE_INVALID;
		}
		else if (!this.validateElement(parts[3], RegexConfig.SIZE_REGEX)) {
			message = MessageConfig.SIZE_INVALID;
		}
		else {
			message = MessageConfig.REFERENCE_VALID;
		}
		return message;
		
	}
	
	/**
	 * @param s
	 * 			Element of reference.
	 * @param regex
	 * 			Regular expression.
	 * @return If the element is correct or not.
	 */
	private boolean validateElement(String s, String regex) {
		
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(s).matches();
		
	}
	
	public void feedReport(Report report, String str, int lineNb) {
		
		String message = this.validateReference(str);
    	if (MessageConfig.REFERENCE_VALID.equals(message)) {
    		String[] parts = str.split(";");
        	report.getReferences().add(new Reference(parts[0], Integer.parseInt(parts[3]), Double.parseDouble(parts[2]), parts[1]));
    	}
    	else {
    		report.getErrors().add(new Mistake(lineNb, message, str));
    	}
    	
	}

	public void convertTXTToXMLOrJSON(String source, String format, String destination) throws IOException, JAXBException {
		
		File f = new File(source);
		Report report = new Report(f.getName(), new ArrayList<Reference>(), new ArrayList<Mistake>());
		
		FileInputStream inputStream = new FileInputStream(source);
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		
		String str = null;
		int lineNb = 1;
		while((str = bufferedReader.readLine()) != null)
		{
			this.feedReport(report, str, lineNb);
	    	lineNb++;
		}
		
		inputStream.close();
		bufferedReader.close();
		
		if ("XML".equals(format)) {
			this.writeXMLFile(report, destination);
		}
		else if ("JSON".equals(format)) {
			this.writeJSONFile(report, destination);
		}
		else {
			 System.out.println("Incorrect format to convert" );
		}
		
	}
	
	/**
	 * Convert report object to XML and write it as a file.
	 * 
	 * @param report
	 * 			Report object.
	 * @param destination
	 * 			Output file path.
	 * @throws JAXBException
	 */
	private void writeXMLFile(Report report, String destination) throws JAXBException {

        // create JAXB context and instantiate marshaller
        JAXBContext context = JAXBContext.newInstance(Report.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        // Write to File
        m.marshal(report, new File(destination));
        System.out.println("File " + new File(destination).getAbsolutePath() + " is created successfully");
        System.out.println("----------");
        // Write to System.out
        m.marshal(report, System.out);
	
	}
	
	/**
	 * Convert report object to JSON and write it as a file.
	 * 
	 * @param report
	 * 			Report object.
	 * @param destination
	 * 			Output file path.
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private void writeJSONFile(Report report, String destination) throws JsonGenerationException, JsonMappingException, IOException {

    	ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
    	// Write to File
    	writer.writeValue(new File(destination), report);
        System.out.println("File " + new File(destination).getAbsolutePath() + " is created successfully");
        System.out.println("----------");
        // Write to System.out
        String jsonStr = writer.writeValueAsString(report);
        System.out.println(jsonStr); 
		
	}

}
