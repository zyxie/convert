package com.miage.convert.app;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import com.miage.convert.service.ReportServiceInterface;
import com.miage.convert.implementation.ReportServiceImplementation;

public class App 
{
    public static void main( String[] args ) throws IOException, JAXBException
    {
    	
    	String source = args[0];
    	String format = args[1];
    	String destination = args[2];
    	ReportServiceInterface reportService = new ReportServiceImplementation();
    	reportService.convertTXTToXMLOrJSON(source, format, destination);
        
    }
}
