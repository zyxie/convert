package com.miage.convert.config;

/**
 * Invalid or valid message.
 * 
 * @author zyxie
 *
 */
public class MessageConfig {
	
	public static final String NUMELEMENTS_INVALID = "Incorrect number of elements for reference";
	public static final String NUMREFERENCE_INVALID = "Incorrect value for numReference";
	public static final String COLOR_INVALID = "Incorrect value for color";
	public static final String PRICE_INVALID = "Incorrect value for price";
	public static final String SIZE_INVALID = "Incorrect value for size";
	public static final String REFERENCE_VALID = "Valid reference";

}
