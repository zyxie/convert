package com.miage.convert.config;

/**
 * Regular expressions to verify each element of reference.
 * 
 * @author zyxie
 *
 */
public class RegexConfig {

	/**
	 * Possible Color Values: R(Red), G(Green), B(Blue).
	 */
	public static final String COLOR_REGEX = "R|G|B";
	/**
	 * Reference number must have 10 digits.
	 */
	public static final String NUMREFERENCE_REGEX = "^\\d{10}";
	/**
	 * Price in euro.
	 */
	public static final String PRICE_REGEX = "^[0-9]+(\\.[0-9]+)?";
	/**
	 * Size must have an integer value.
	 */
	public static final String SIZE_REGEX = "^\\d+";
	
}
