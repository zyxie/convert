package com.miage.convert.service;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.miage.convert.model.Report;

public interface ReportServiceInterface {
	
	/**
	 * Validate if a reference is correct.
	 * 
	 * @param str
	 * 			Reference line.
	 * @return Valid or invalid message.
	 */
	String validateReference(String str);
	
	/**
	 * Provide report object its reference or error elements.
	 * 
	 * @param report
	 * 			Report object.
	 * @param str
	 * 			Reference line.
	 * @param lineNb
	 * 			Reference line number.
	 */
	void feedReport(Report report, String str, int lineNb);
	
	/**
	 * Convert TXT file to XML file or JSON file.
	 * 
	 * @param source
	 * 			Path of the file to convert.
	 * @param format
	 * 			Format to convert.
	 * @param destination
	 * 			Output file path.
	 * @throws IOException
	 * @throws JAXBException
	 */
	void convertTXTToXMLOrJSON(String source, String format, String destination) throws IOException, JAXBException;

}
