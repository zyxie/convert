package com.miage.convert.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

// Define the root element for an XML tree
@XmlRootElement(name = "report")
// Define the order in which the fields are written
@XmlType(propOrder = { "inputFile", "references", "errors" })
public class Report {
	
	private String inputFile;
	private List<Reference> references;
	private List<Mistake> errors;
	
	public Report() {
		super();
	}

	public Report(String inputFile, List<Reference> references, List<Mistake> errors) {
		super();
		this.inputFile = inputFile;
		this.references = references;
		this.errors = errors;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	// XmLElementWrapper generates a wrapper element around XML representation
	@XmlElementWrapper(name="references")
	// XmlElement sets the name of the entities
    @XmlElement(name="reference")
	public List<Reference> getReferences() {
		return references;
	}

	public void setReferences(List<Reference> references) {
		this.references = references;
	}

	@XmlElementWrapper(name="errors")
    @XmlElement(name="error")
	public List<Mistake> getErrors() {
		return errors;
	}

	public void setErrors(List<Mistake> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "Information [inputFile=" + inputFile + ", references=" + references + ", errors=" + errors + "]";
	}
	
}
