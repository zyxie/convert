package com.miage.convert.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "reference")
@XmlType(propOrder = { "type", "price", "size", "numReference" })
public class Reference {
	
	private String numReference;
	private int size;
	private double price;
	private String type;
	
	public Reference() {
		super();
	}

	public Reference(String numReference, int size, double price, String type) {
		super();
		this.numReference = numReference;
		this.size = size;
		this.price = price;
		this.type = type;
	}

	@XmlAttribute
	public String getNumReference() {
		return numReference;
	}

	public void setNumReference(String numReference) {
		this.numReference = numReference;
	}

	@XmlAttribute
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@XmlAttribute
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

    // Use color as name of type attribute for XML-Output
	@XmlAttribute(name = "color")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Reference [numReference=" + numReference + ", size=" + size + ", price=" + price + ", type=" + type
				+ "]";
	}
	
}
