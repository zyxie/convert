package com.miage.convert.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "error")
public class Mistake {
	
	private int line;
	private String message;
	private String value;
	
	public Mistake() {
		super();
	}
	
	public Mistake(int line, String message, String value) {
		super();
		this.line = line;
		this.message = message;
		this.value = value;
	}

	@XmlAttribute
	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	@XmlAttribute
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@XmlValue
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Mistake [line=" + line + ", message=" + message + ", value=" + value + "]";
	}
	
}
