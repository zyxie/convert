# Project Convert

Convert TXT file to JSON or XML file

**Build**
Run mvn install from the command line

**Utilisation**

Use convert-0.0.1-SNAPSHOT-jar-with-dependencies.jar which is generated in target:
java -jar convert-0.0.1-SNAPSHOT-jar-with-dependencies.jar TXT_file_path format(XML/JSON) output_file_path
Exemples:
java -jar convert-0.0.1-SNAPSHOT-jar-with-dependencies.jar input.txt XML output.xml
java -jar convert-0.0.1-SNAPSHOT-jar-with-dependencies.jar input.txt JSON output.json